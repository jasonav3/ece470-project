# ECE470 Project

Code for ece470 final project


**How to run:**

- Pull Code

- Open 2 terminals
- "cd PATH/catkin_jasonav3" in both terminals
- run "catkin_make" and "source devel/setup.bash" in both terminals

- Terminal 1 Run: 
    1) "roslaunch ur3_driver ur3_gazebo.launch"


- Terminal 2 Run: 

    1) rosrun lab2pkg_py lab2_spawn.py

        1a) enter any position

        
        1b) Type 'n' for "Missing Block?" prompt

    2)rosrun lab5pkg_py lab5exec.py --simulator True
